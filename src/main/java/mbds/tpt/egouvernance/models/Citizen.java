package mbds.tpt.egouvernance.models;

import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.UniqueConstraint;
import lombok.Getter;
import lombok.Setter;
import mbds.tpt.egouvernance.models.enums.EGenre;

@Entity
@Getter
@Setter
@Table(name = "citoyens", uniqueConstraints = { @UniqueConstraint(columnNames = { "nom", "prenom" }) }, indexes = {
		@Index(name = "idx_citoyen_nom", columnList = "nom"),
		@Index(name = "idx_citoyen_prenom", columnList = "prenom") })
public class Citizen {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "cin", unique = true)
	private String nic;

	@Column(name = "nom")
	private String firstName;

	@Column(name = "prenom")
	private String lastName;

	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private EGenre genre;

	@Temporal(TemporalType.DATE)
	@Column(name = "dateNaissance")
	private Date birthDate;

	@Column(name = "adresse")
	private String adress;

	@OneToMany(mappedBy = "citizen")
	@JsonIgnore
	private Set<DocRequest> docRequests;

	@ManyToMany
	@JoinTable(name = "parent_enfant", joinColumns = @JoinColumn(name = "enfant_id"), inverseJoinColumns = @JoinColumn(name = "parent_id"))
	@JsonIgnore
	private Set<Citizen> parents;

	public void addParent(Citizen parent) {
		if (!this.parents.contains(parent)) {
			this.parents.add(parent);
		}
	}

	public void removeParent(Citizen parent) {
		this.parents.remove(parent);
	}

}
