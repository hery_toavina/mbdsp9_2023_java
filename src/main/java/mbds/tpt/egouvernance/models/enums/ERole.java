package mbds.tpt.egouvernance.models.enums;

public enum ERole {
	ROLE_ADMIN,
	ROLE_MODERATOR,
	ROLE_USER
}
