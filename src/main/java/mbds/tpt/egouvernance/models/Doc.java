package mbds.tpt.egouvernance.models;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "documents", indexes = { @Index(name = "idx_document_nom", columnList = "nom"), @Index(name = "idx_document_code", columnList = "code")})
public class Doc {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(unique=true)
	private String code;
	
	@Column(name = "nom", unique = true)
	private String name;

	private String description;

	@OneToMany(mappedBy = "doc")
	@JsonIgnore
	private Set<DocRequest> docRequests;
}
