package mbds.tpt.egouvernance.models;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mbds.tpt.egouvernance.models.enums.EStatut;

@Entity
@Getter
@Setter
@Table(name = "demandes", indexes = { @Index(name = "idx_demande_statut", columnList = "statut") })
public class DocRequest {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Temporal(TemporalType.DATE)
	@Column(name = "dateDemande")
	private Date requestDate;

	@Enumerated(EnumType.STRING)
	@Column(name = "statut")
	private EStatut status;

	@Column(name = "commentaires")
	private String comments;
	
	@Column(name="isTelecharger")
	private Boolean isDownloaded;

	@ManyToOne
	@JoinColumn(name = "citoyen_id", nullable = false)
	private Citizen citizen;
	
	@ManyToOne
	@JoinColumn(name= "user_id", nullable= false)
	private User user;

	@ManyToOne
	@JoinColumn(name = "document_id", nullable = false)
	private Doc doc;

}
