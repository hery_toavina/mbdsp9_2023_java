package mbds.tpt.egouvernance.models;

public enum ERole {
	ROLE_ADMIN,
	ROLE_MODERATOR,
	ROLE_USER
}
