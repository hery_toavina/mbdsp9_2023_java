package mbds.tpt.egouvernance.models.DTO;

import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mbds.tpt.egouvernance.models.Citizen;
import mbds.tpt.egouvernance.models.enums.EGenre;

@Getter
@Setter
@NoArgsConstructor
public class CitizenDTO {
	private Long id;

	private String nic;

	private String firstName;

	private String lastName;

	private EGenre genre;

	private Date birthDate;

	private String adress;

	private Set<DocRequestDTO> docRequests;
	
	private Set<CitizenDTO> parents;
   // private Set<CitizenDTO> children;
    
    public static CitizenDTO mapToDTO(Citizen citizen) {
        CitizenDTO dto = new CitizenDTO();
        dto.setId(citizen.getId());
        dto.setNic(citizen.getNic());
        dto.setFirstName(citizen.getFirstName());
        dto.setLastName(citizen.getLastName());
        dto.setGenre(citizen.getGenre());
        dto.setBirthDate(citizen.getBirthDate());
        dto.setAdress(citizen.getAdress());
        return dto;
    }
    
    public static CitizenDTO mapToDTOWithParents(Citizen citizen) {
        CitizenDTO dto = new CitizenDTO();
        dto.setId(citizen.getId());
        dto.setNic(citizen.getNic());
        dto.setFirstName(citizen.getFirstName());
        dto.setLastName(citizen.getLastName());
        dto.setGenre(citizen.getGenre());
        dto.setBirthDate(citizen.getBirthDate());
        dto.setAdress(citizen.getAdress());
        Set<CitizenDTO> parentsDTO = citizen.getParents().stream().map(CitizenDTO::mapToDTO)
				.collect(Collectors.toSet());
		dto.setParents(parentsDTO);
        return dto;
    }
}
