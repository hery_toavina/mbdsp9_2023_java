package mbds.tpt.egouvernance.models.DTO;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import mbds.tpt.egouvernance.models.User;

@Getter
@Setter
public class UserDTO {
	private Long id;

	private String username;

	private String email;

	private Set<DocRequestDTO> docRequests;
	
	public static UserDTO mapToDTO(User user) {
		UserDTO dto = new UserDTO();
		dto.setId(user.getId());
		dto.setUsername(user.getUsername());
		dto.setEmail(user.getEmail());
		return dto;
	}
}
