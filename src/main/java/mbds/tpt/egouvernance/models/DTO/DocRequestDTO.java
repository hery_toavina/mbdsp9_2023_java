package mbds.tpt.egouvernance.models.DTO;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import mbds.tpt.egouvernance.models.DocRequest;
import mbds.tpt.egouvernance.models.enums.EStatut;

@Getter
@Setter
public class DocRequestDTO {
	private Long id;

	private Date requestDate;

	private EStatut status;

	private String comments;

	private String citizen;

	private String doc;
	
	private String user;
	
	public static DocRequestDTO mapToDTO(DocRequest docRequest) {
		DocRequestDTO dto = new DocRequestDTO();
		dto.setId(docRequest.getId());
		dto.setRequestDate(docRequest.getRequestDate());
		dto.setComments(docRequest.getComments());
		dto.setStatus(docRequest.getStatus());
		dto.setDoc(docRequest.getDoc().getName());
		dto.setCitizen(docRequest.getCitizen().getFirstName()  + " " + docRequest.getCitizen().getLastName());
		dto.setUser(docRequest.getUser().getUsername());
		return dto;
	}
	
}
