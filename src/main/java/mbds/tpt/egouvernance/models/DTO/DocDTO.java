package mbds.tpt.egouvernance.models.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DocDTO {
	private Long id;

	private String name;
	
	private String code;

	private String description;

}
