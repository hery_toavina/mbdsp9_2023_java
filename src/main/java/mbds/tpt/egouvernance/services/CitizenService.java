package mbds.tpt.egouvernance.services;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mbds.tpt.egouvernance.models.Citizen;
import mbds.tpt.egouvernance.models.DTO.CitizenDTO;
import mbds.tpt.egouvernance.payload.request.CitizenReq;
import mbds.tpt.egouvernance.repository.CitizenRepository;

@Service
public class CitizenService {

	@Autowired
	private CitizenRepository cr;

	public CitizenDTO save(CitizenReq citizenReq) {
		Citizen newCitizen = new Citizen();
		newCitizen.setParents(new HashSet<Citizen>());

		newCitizen.setNic(citizenReq.getNic());
		newCitizen.setFirstName(citizenReq.getFirstName());
		newCitizen.setLastName(citizenReq.getLastName());
		newCitizen.setGenre(citizenReq.getGenre());
		newCitizen.setBirthDate(citizenReq.getBirthDate());
		newCitizen.setAdress(citizenReq.getAdress());
		if (citizenReq.getParentIds() != null && citizenReq.getParentIds().size() > 0) {
			Set<Citizen> parent = citizenReq.getParentIds().stream().map(this::findById).collect(Collectors.toSet());
			for (Citizen p : parent)
				newCitizen.addParent(p);
		}
		newCitizen = cr.save(newCitizen);
		return CitizenDTO.mapToDTO(newCitizen);
	}

	public Citizen findById(Long id) {
		return cr.findById(id).orElse(null);
	}
	
	public Citizen findByNic(String nic) {
		return cr.findByNic(nic).orElse(null);
	}
	
	public CitizenDTO findCitizenDetails(String nic) {
		Citizen citizen = cr.findByNic(nic).orElse(null);
		if (citizen != null) {
			CitizenDTO dto = new CitizenDTO();
			dto = CitizenDTO.mapToDTOWithParents(citizen);

			/*Set<CitizenDTO> parentsDTO = citizen.getParents().stream().map(CitizenDTO::mapToDTO)
					.collect(Collectors.toSet());
			dto.setParents(parentsDTO);*/

			return dto;
		}
		return null;
	}

	public CitizenDTO findCitizenDetails(Long id) {
		Citizen citizen = cr.findById(id).orElse(null);
		if (citizen != null) {
			CitizenDTO dto = new CitizenDTO();
			dto = CitizenDTO.mapToDTOWithParents(citizen);

			/*Set<CitizenDTO> parentsDTO = citizen.getParents().stream().map(CitizenDTO::mapToDTO)
					.collect(Collectors.toSet());
			dto.setParents(parentsDTO);*/

			return dto;
		}
		return null;
	}

	public Set<CitizenDTO> findAll() {
		Set<CitizenDTO> citizens = new LinkedHashSet<>();
		citizens = cr.findAll().stream().map(CitizenDTO::mapToDTO).collect(Collectors.toSet());
		return citizens;
	}
}
