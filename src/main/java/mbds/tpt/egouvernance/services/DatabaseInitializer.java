package mbds.tpt.egouvernance.services;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import mbds.tpt.egouvernance.models.Role;
import mbds.tpt.egouvernance.models.enums.ERole;
import mbds.tpt.egouvernance.repository.RoleRepository;

@Component
public class DatabaseInitializer implements CommandLineRunner {
	private RoleRepository roleRepository;
	
	@Autowired
    public DatabaseInitializer(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        if (roleRepository.count() == 0) {
            Role role_admin = new Role(), role_user = new Role(), role_mod = new Role();
            role_admin.setName(ERole.ROLE_ADMIN); role_user.setName(ERole.ROLE_USER);role_mod.setName(ERole.ROLE_MODERATOR);
            this.roleRepository.saveAll(Arrays.asList(role_admin, role_user, role_mod));
        }
    }
}
