package mbds.tpt.egouvernance.services;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mbds.tpt.egouvernance.models.Doc;
import mbds.tpt.egouvernance.repository.DocRepository;

@Service
public class DocService {
	
	@Autowired
	private DocRepository dr;
	
	public Doc save(Doc doc) {
		return dr.save(doc);
	}
	
	public Set<Doc> findAll() {
		return dr.findAll().stream().collect(Collectors.toSet());
	}
	
	public Doc findById(Long id) {
		return dr.findById(id).orElse(null);
	}
	
	public Doc findByCode(String code) {
		return dr.findByCode(code).orElse(null);
	}
}
