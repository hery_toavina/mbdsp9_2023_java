package mbds.tpt.egouvernance.services;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mbds.tpt.egouvernance.exception.CitizenException;
import mbds.tpt.egouvernance.exception.DocException;
import mbds.tpt.egouvernance.models.Citizen;
import mbds.tpt.egouvernance.models.Doc;
import mbds.tpt.egouvernance.models.DocRequest;
import mbds.tpt.egouvernance.models.User;
import mbds.tpt.egouvernance.models.DTO.DocRequestDTO;
import mbds.tpt.egouvernance.models.DTO.UserDTO;
import mbds.tpt.egouvernance.models.enums.EStatut;
import mbds.tpt.egouvernance.repository.DocRequestRepository;
import mbds.tpt.egouvernance.repository.UserRepository;
import mbds.tpt.egouvernance.security.jwt.JwtUtils;

@Service
public class DocRequestService {

	@Autowired
	private DocRequestRepository drr;
	@Autowired
	private CitizenService cs;
	@Autowired
	private DocService ds;
	@Autowired
	private JwtUtils jwtUtils;
	@Autowired
	private UserRepository ur;

	public DocRequestDTO save(mbds.tpt.egouvernance.payload.request.DocRequest docRequest, String token) {
		DocRequest newDocRequest = new DocRequest();
		newDocRequest.setRequestDate(new Date());
		newDocRequest.setStatus(EStatut.TERMINE);

		token = token.split(" ")[1];
		String username = jwtUtils.getUserNameFromJwtToken(token);
		User user = ur.findByUsername(username).orElse(null);

		Citizen citizen = cs.findByNic(docRequest.getCitizen());
		if (citizen == null)
			throw new CitizenException("CIN non reconnu, utilisateur introuvable.");
		Doc doc = ds.findByCode(docRequest.getDoc());
		if (doc == null)
			throw new DocException("Document introuvable.");

		newDocRequest.setUser(user);
		newDocRequest.setCitizen(citizen);
		newDocRequest.setDoc(doc);
		newDocRequest = drr.save(newDocRequest);
		return DocRequestDTO.mapToDTO(newDocRequest);
	}

	public UserDTO findUserRequests(String token) {
		token = token.split(" ")[1];
		String username = jwtUtils.getUserNameFromJwtToken(token);
		User user = ur.findByUsername(username).orElse(null);
		UserDTO dto = UserDTO.mapToDTO(user);
		Set<DocRequestDTO> docRequests = user.getDocRequests().stream().map(DocRequestDTO::mapToDTO)
				.collect(Collectors.toSet());
		dto.setDocRequests(docRequests);
		return dto;
	}
	
	public Set<DocRequestDTO> findAll(){
		Set<DocRequestDTO> requests = new LinkedHashSet<>();
		requests = drr.findAll().stream().map(DocRequestDTO::mapToDTO).collect(Collectors.toSet());
		return requests;
		
	}
}
