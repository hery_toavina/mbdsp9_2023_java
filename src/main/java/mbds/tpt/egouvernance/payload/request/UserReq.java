package mbds.tpt.egouvernance.payload.request;

import java.util.Set;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserReq {
	@NotBlank
	//@Size(min = 3, max = 20)
	private String username;

	@NotBlank
	//@Size(max = 50)
	@Email
	private String email;
	
	 @NotBlank
	 private String password;

	private Set<String> role;
}
