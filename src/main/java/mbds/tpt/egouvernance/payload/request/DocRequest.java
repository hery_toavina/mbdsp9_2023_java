package mbds.tpt.egouvernance.payload.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DocRequest {
	private String citizen;
	private String doc;
}
