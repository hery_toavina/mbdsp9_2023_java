package mbds.tpt.egouvernance.payload.request;

import java.util.Date;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import mbds.tpt.egouvernance.models.enums.EGenre;

@Getter
@Setter
public class CitizenReq {
	private String nic;

	private String firstName;

	private String lastName;

	private EGenre genre;

	private Date birthDate;

	private String adress;

	private Set<Long> parentIds;
}
