package mbds.tpt.egouvernance.exception;

@SuppressWarnings("serial")
public class CitizenException extends RuntimeException {
    public CitizenException(String errorMessage) {
        super(errorMessage);
    }
}
