package mbds.tpt.egouvernance.exception;

@SuppressWarnings("serial")
public class DocException extends RuntimeException {
    public DocException(String errorMessage) {
        super(errorMessage);
    }
}
