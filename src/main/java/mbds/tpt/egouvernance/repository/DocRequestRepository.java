package mbds.tpt.egouvernance.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mbds.tpt.egouvernance.models.DocRequest;

public interface DocRequestRepository extends JpaRepository<DocRequest, Long> {

}
