package mbds.tpt.egouvernance.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import mbds.tpt.egouvernance.models.Citizen;

public interface CitizenRepository extends JpaRepository<Citizen, Long>{
	Optional<Citizen> findByNic(String nic);
}
