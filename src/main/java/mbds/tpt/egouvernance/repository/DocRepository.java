package mbds.tpt.egouvernance.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import mbds.tpt.egouvernance.models.Doc;

public interface DocRepository extends JpaRepository<Doc, Long>{
	Optional<Doc> findByCode(String code);
}
