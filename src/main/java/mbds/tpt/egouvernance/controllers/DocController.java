package mbds.tpt.egouvernance.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mbds.tpt.egouvernance.models.Doc;
import mbds.tpt.egouvernance.services.DocService;

@RestController
@RequestMapping("tpt/api/doc")
public class DocController {
	
	@Autowired
	private DocService ds;
	
	@GetMapping("/")
	public ResponseEntity<?> findAll() throws RuntimeException {
		return ResponseEntity.ok(ds.findAll());
	}
	
	@PostMapping("/")
	public ResponseEntity<?> save(@RequestBody Doc doc) throws RuntimeException {
		return ResponseEntity.ok(ds.save(doc));
	} 
}
