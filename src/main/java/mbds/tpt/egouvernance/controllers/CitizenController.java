package mbds.tpt.egouvernance.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mbds.tpt.egouvernance.payload.request.CitizenReq;
import mbds.tpt.egouvernance.services.CitizenService;

@RestController
@RequestMapping("/tpt/api/citizen")
public class CitizenController {

	@Autowired
	private CitizenService cs;
	
	@PostMapping("/")
	public ResponseEntity<?> save(@RequestBody CitizenReq citizenReq){
		return ResponseEntity.ok(cs.save(citizenReq));
	}
	
	@GetMapping("/")
	public ResponseEntity<?> findAll(){
		return ResponseEntity.ok(cs.findAll());
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findDetails(@PathVariable Long id){
		return ResponseEntity.ok(cs.findCitizenDetails(id));
	}
	
	@GetMapping("/nic/{nic}")
	public ResponseEntity<?> findDetailsByNic(@PathVariable String nic){
		return ResponseEntity.ok(cs.findCitizenDetails(nic));
	}
	
}
