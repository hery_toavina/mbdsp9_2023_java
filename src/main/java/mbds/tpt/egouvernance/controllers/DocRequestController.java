package mbds.tpt.egouvernance.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mbds.tpt.egouvernance.payload.request.DocRequest;
import mbds.tpt.egouvernance.services.DocRequestService;

@RestController
@RequestMapping("tpt/api/request")
public class DocRequestController {
	@Autowired
	private DocRequestService drs;
	
	@GetMapping("/all")
	public ResponseEntity<?> getAllRequest(){
		return ResponseEntity.ok(drs.findAll());
	}
	
	
	@GetMapping("/")
	public ResponseEntity<?> getRequest(@RequestHeader (name="Authorization") String token){
		return ResponseEntity.ok(drs.findUserRequests(token));
	}
	
	@PostMapping("/")
	public ResponseEntity<?> addRequest(@RequestBody DocRequest docRequest, @RequestHeader (name="Authorization") String token){
		return ResponseEntity.ok(drs.save(docRequest, token));
	}
}
