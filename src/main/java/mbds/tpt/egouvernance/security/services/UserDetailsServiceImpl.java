package mbds.tpt.egouvernance.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import mbds.tpt.egouvernance.models.User;
import mbds.tpt.egouvernance.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	  @Autowired
	  UserRepository userRepository;

	  @Override
	  @Transactional
	  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	    User user = userRepository.findByUsername(username)
	        .orElseThrow(() -> new UsernameNotFoundException(username + " Introuvable !!"));

	    return UserDetailsImpl.build(user);
	  }

	}
